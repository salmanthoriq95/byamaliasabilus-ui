import {useEffect, useState} from 'react'

const useFetch = (url) => {
	const [data, setData] = useState(null);
    const [load, setLoad] = useState(true);
    const [error, setError] = useState(null);

   useEffect(() => {
    const abortC = new AbortController();

    
        fetch (url, {signal : abortC.signal})
            .then(res => {
                if(!res.ok){
                    throw Error('could not fetch the data for that resource')
                }
                return res.json();
            })
            .then(data => {
                setData(data.data);
                setLoad(false);
                setError(null);
            })
            .catch(err=>{
                if(err.name === 'AbortError'){
                    console.log('fetch aborted');
                } else {
                    setLoad(false);
                    setError(err.message);
                }
            })
    

    return ()=>abortC.abort();
   },[url])
   return {data, load, error}
}

export default useFetch;