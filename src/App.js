import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import {ShowArticle, Home, Navbar, Article,  Profile, Services, Gallery, NotFound, Footer} from './components/index';

function App() {
    return (

        <Router>
            <div className="App h-100">
                <Navbar />

                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                
                    <Route path="/article/:id">
                        <ShowArticle />
                    </Route>
                
                    <Route path="/article">
                        <Article />
                    </Route>
                
                    <Route path="/gallery">
                        <Gallery />
                    </Route>
                
                    <Route path="/profile">
                        <Profile />
                    </Route>
                
                    <Route path="/services">
                        <Services />
                    </Route>
                
                    <Route path="*">
                        <NotFound />
                    </Route>                
                </Switch>
                
                <Footer />
            </div>
        </Router>
    );
    }

export default App;
