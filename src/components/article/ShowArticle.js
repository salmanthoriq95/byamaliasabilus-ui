import useFetch from "../../controller/useFetch";
import {useParams} from "react-router";
import { Link } from "react-router-dom";

const ShowArticle = () => {

	const {id} = useParams();
	const {data: blog, error, load} = useFetch('http://localhost:3001/article?id='+id);
	const {data: newBlog} = useFetch('http://localhost:3001/article?id='+(Number(id)+1));
	const {data: oldBlog} = useFetch('http://localhost:3001/article?id='+(Number(id)-1));
		
	

	return (
    	<div className="show-article">
			{error && <div>{ error }</div>}
            {load && <div>Loading . . . </div>}
            {blog && (
				<main className="container text-start">
					<div className="row my-5">
						<div className="col-md-8 mt-5">
							<article className="blog-post">
								<h2 className="blog-post-title text-center fw-bold display-4">{blog.title}</h2>
								<p className="blog-post-meta text-center">{blog.date.day+" "+blog.date.month+" "+blog.date.year+" by "+blog.author}</p>
								<img src={blog.thumbnail} className="img-fluid d-block mx-auto" alt="thumbnail" style={{width:350}}></img>
								<p className="text-center fst-italic mt-4">{blog.short}</p>
								<hr />
								<p>{blog.content}</p>
							</article>
						</div>
						<div className="col-md-3 g-3">
							
                                {newBlog && (
								<div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative mt-5 p-2 ">
									<div className="container-fluid bg-primary my-2 fw-bold fst-italic text-white text-center">Newer</div>
									<img src={newBlog.thumbnail} alt="thumbnail" width="200" className="d-none d-lg-block"></img>
                                    <div className="col p-4 d-flex flex-column position-static">
                                        
                                        <h3 className="mb-0 text-center">{newBlog.title}</h3>
                                        <div className="mb-1 teblogt-muted text-center">{newBlog.date.day} {newBlog.date.month} {newBlog.date.year}</div>
                                        <p className="card-teblogt mb-auto">{newBlog.short}</p>
                                        <Link to={"/article/"+newBlog.id} className="stretched-link">Continue reading</Link>
                                    </div> 
								</div>)}

								{oldBlog && (<div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative mt-5 p-2 ">
								<div className="container-fluid bg-secondary my-2 fw-bold fst-italic text-white text-center">Older</div>
									<img src={oldBlog.thumbnail} alt="thumbnail" width="200" className="d-none d-lg-block"></img>
                                    <div className="col p-4 d-flex flex-column position-static">
                                        
                                        <h3 className="mb-0 text-center">{oldBlog.title}</h3>
                                        <div className="mb-1 teblogt-muted text-center">{oldBlog.date.day} {oldBlog.date.month} {oldBlog.date.year}</div>
                                        <p className="card-teblogt mb-auto">{oldBlog.short}</p>
                                        <Link to={"/article/"+oldBlog.id} className="stretched-link">Continue reading</Link>
                                    </div> 
								</div>)}
                            
						</div>
					</div>
				</main>

            )}
    	</div>
    )
}

export default ShowArticle;