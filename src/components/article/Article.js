import { Link } from "react-router-dom";
import useFetch from "../../controller/useFetch";

const Article = () => {
    
    const {data: blog, error, load} = useFetch('http://localhost:3001/article/');
    
    return (
        
        <div className="article shadow-sm p-3 mb-5 bg-white rounded mt-5">
            {error && <div>{ error }</div>}
            {load && <div>Loading . . . </div>}
            {blog && (  
                
                <main className="container">
                    <div key={blog[0].id} className="p-4 p-md-5 mb-4 text-white rounded bg-dark" style={{backgroundImage: 'url(' + blog[0].thumbnail + ')', backgroundRepeat:'no-repeat',backgroundSize:'100%'}} >
                        <div className="col-md-6 px-0">
                            <h1 className="display-4 fst-italic">{blog[0].title}</h1>
                            <p className="lead my-3">{blog[0].short}</p>
                            <p className="lead mb-0"><Link to={"/article/"+blog[0].id}className="text-white fw-bold">Continue reading...</Link></p>
                        </div>
                    </div>

                    <div className="row mb-2">
                        {blog.map(x=>(
                            <div key={x.id} className="col-md-6">
                                <div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                                    <div className="col p-4 d-flex flex-column position-static">
                                      
                                        <h3 className="mb-0">{x.title}</h3>
                                        <div className="mb-1 text-muted">{x.date.day} {x.date.month} {x.date.year}</div>
                                        <p className="card-text mb-auto">{x.short}</p>
                                        <Link to={"/article/"+x.id} className="stretched-link">Continue reading</Link>
                                    </div>
                                    <div className="col-auto d-none d-lg-block">
                                        <svg className="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><image href={x.thumbnail} width="200"></image></svg>
                                        
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </main>
            )}
        </div>
    );
}
 
export default Article;