import useFetch from "../../controller/useFetch";

const Gallery = () => {
	const {data: gallery, error, load} = useFetch('http://localhost:3001/gallery');

	return (
	    <div className="gallery">
	        {error && <div>{ error }</div>}
            {load && <div>Loading . . . </div>}
            {gallery && (
            	<main>
					<section className="py-5 text-center container">
						<div className="row py-lg-5">
							<div className="col-lg-6 col-md-8 mx-auto">
								<h1 className="fw-light">Album example</h1>
								<p className="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
							</div>
						</div>
					</section>

					<div className="album py-5 bg-light">
						<div className="container">

							<div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
								{gallery.map(x=>(
									<div key={x.id} className="col">
									<div className="card shadow-sm">
										<img src={x.img} alt="" className="bd-placeholder-img card-img-top"></img>
										<div className="card-body">
											<p className="card-text">{x.desc}</p>
											<div>
												<button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target={"#exampleModal"+x.id}>
													Launch demo modal
												</button>												
											</div>
											<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-geo-alt-fill" viewBox="0 0 16 16">
												<path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>

											</svg> 
											<span>{x.loc}</span>
											<div className="modal fade " id={"exampleModal"+x.id} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div className="modal-dialog modal-dialog-centered">
													<div className="modal-content" style={{width:"100%"}}>
														<div className="modal-header">				
															<button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
														</div>
														<div className="modal-body">
															<img src={x.img} alt="zoom" className="w-100 img-fluid"></img>												
														</div>
														<div className="modal-footer">
															<button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>														
														</div>
													</div>
												</div>
											</div>
											<div className="d-flex justify-content-between align-items-center">
												<small className="text-muted">{x.date.day+" "+x.date.month+" "+x.date.year}</small>
											</div>
										</div>
									</div>
								</div>
								))}
							</div>
						</div>
					</div>

				</main>
		)}
		</div>
)}

export default Gallery;