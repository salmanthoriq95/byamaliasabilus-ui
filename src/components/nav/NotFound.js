const NotFound = () => {
    return (
        <div className="not-found h-100">
			<div className="d-flex h-100 text-center text-white bg-dark">

				<div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
				
					<main className="position-absolute top-50 start-50 translate-middle">
						<h1>Not Found - 404</h1>
						<p className="lead">So sorry, the page you ask is not exist. Please check your URL</p>
						<p className="lead">
							<a href="/" className="btn btn-lg btn-primary fw-bold">Back Home</a>
						</p>
					</main>
				</div>



			</div>
        </div>
    );
}
 
export default NotFound;