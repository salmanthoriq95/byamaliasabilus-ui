module.exports = {
	Article : require('./article/Article').default,
	ShowArticle : require('./article/ShowArticle').default,
	Gallery : require('./gallery/Gallery').default,
	Home : require('./home/Home').default,
	Footer : require('./nav/Footer').default,
	Navbar : require('./nav/Navbar').default,
	NotFound : require('./nav/NotFound').default,
	Profile : require('./profile/Profile').default,
	Services : require('./services/Services').default
}