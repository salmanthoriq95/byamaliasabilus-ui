import { Link } from "react-router-dom";
import useFetch from "../../controller/useFetch";

const Home = () => {
    const {data: service} = useFetch('http://localhost:3001/services');
    const {data: article, error, load} = useFetch('http://localhost:3001/article');
    return (
        <div className="home">
            {error && <div>{ error }</div>}
            {load && <div>Loading . . . </div>}
            {article && (
                <div className="parent">
                    <div id="myCarousel" className="carousel slide mt-5 " data-bs-ride="carousel" style={{height:'500px'}} >
                        <div className="carousel-indicators">
                            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div className="carousel-inner">
                            <div className="carousel-item active ">
                                <div className="center-cropped">
                                    <img src="/img/card1.jpg" alt="gambar-1"></img>
                                </div>

                                <div className="container">
                                    <div className="carousel-caption text-start top-50 translate-middle-y">
                                        <h1>Main Offer</h1>
                                        <p>Some representative placeholder content for the first slide of the carousel.</p>
                                        <p>
                                            <a className="btn btn-lg btn-primary" href="http://cutt.ly/Ijq6Fmy">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" className="bi bi-whatsapp mx-2 pb-2" viewBox="0 0 16 16">
                                                    <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/>
                                                </svg>
                                                <span className="fs-3 d-none d-xl-inline-block .d-xxl-inline-block">Contact Me</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="center-cropped">
                                    <img src="/img/2.jpg" alt="gambar-1"></img>
                                </div>

                                <div className="container">
                                    <div className="carousel-caption">
                                        <h1>Go to instagram</h1>
                                        <p>Some representative placeholder content for the second slide of the carousel.</p>
                                        <p>
                                            <a className="btn btn-lg btn-primary" href="https://www.instagram.com/byamaliasabilus/">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" className="bi bi-instagram mx-sm-2 pb-sm-2" viewBox="0 0 16 16">
                                                    <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
                                                </svg>
                                                <span className="fs-3 d-none d-xl-inline-block .d-xxl-inline-block">Follow me</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="center-cropped">
                                    <img src="/img/1.jpg" alt="gambar-1"></img>
                                </div>

                                <div className="container">
                                    <div className="carousel-caption text-end">
                                        <h1>One more for good measure.</h1>
                                        <p>Some representative placeholder content for the third slide of this carousel.</p>
                                        <p>
                                            <Link className="btn btn-lg btn-primary" to="#">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" className="bi bi-columns-gap mx-sm-2 pb-sm-2" viewBox="0 0 16 16">
                                              <path d="M6 1v3H1V1h5zM1 0a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1H1zm14 12v3h-5v-3h5zm-5-1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1h-5zM6 8v7H1V8h5zM1 7a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V8a1 1 0 0 0-1-1H1zm14-6v7h-5V1h5zm-5-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1h-5z"/>
                                            </svg>
                                            <span className="fs-3 d-none d-xl-inline-block .d-xxl-inline-block">My Gallery</span>
                                            </Link>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button className="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Previous</span>
                        </button>
                        <button className="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Next</span>
                        </button>
                    </div>

                        {/* Marketing messaging and featurettes
                        ==================================================
                        Wrap the rest of the page in another container to center all the content. */}

                    <div className="container marketing mt-4">
                        <div className="row">
                            {
                                service.map (x => (
                                     <div className="col-lg-4 mt-2" key={x.id}>
                                        <img src={x.thumbnail} style={{width:'150px', height:'150px'}}alt="thumbnail gambar" className="img-thumbnail rounded-circle rounded"></img>

                                        <h2>{x.type}</h2>
                                        <p>{x.desc}</p>
                                        <p><Link className="btn btn-secondary" to={"/services#"+x.type}>View details &raquo;</Link></p>
                                    </div>
                                ))
                            }
                        </div>
                    


                        {/*START THE FEATURETTES*/}
                        <hr className="featurette-divider"></hr>

                        <div className="row featurette">
                            <div className="col-md-7">
                                <h2 className="featurette-heading">{article[0].title}</h2>
                                <p className="lead">{article[0].short}</p>
                                <Link to="#" className="btn btn-primary stretched-link">Continue Reading</Link>
                            </div>
                            <div className="col-md-5">
                                <img src={article[0].thumbnail} alt="thumbnail" className="mt-2 bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto"></img>
                            </div>

                        </div>

                        <hr className="featurette-divider"></hr>

                        <div className="row featurette">
                            <div className="col-md-7 order-md-2">
                                <h2 className="featurette-heading">{article[1].title}</h2>
                                <p className="lead">{article[1].short}</p>
                                <Link to="#" className="btn btn-primary stretched-link">Continue Reading</Link>
                            </div>
                            <div className="col-md-5 order-md-1"> 
                                <img src={article[1].thumbnail} alt="thumbnail" className="mt-2 bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto"></img>
                            </div>
                        </div>

                        <hr className="featurette-divider"></hr>

                        <div className="row featurette">
                            <div className="col-md-7">
                                <h2 className="featurette-heading">{article[2].title}</h2>
                                <p className="lead">{article[2].short}</p>
                                <Link to="#" className="btn btn-primary stretched-link">Continue Reading</Link>
                            </div>
                            <div className="col-md-5">
                                <img src={article[2].thumbnail} alt="thumbnail" className="mt-2 bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto"></img>
                            </div>
                        </div>
                    
                        <hr className="featurette-divider"></hr>
                    </div>
                    {/*END THE FEATURETTES*/}
                </div>
            )}
        </div>
    );
}
 
export default Home;